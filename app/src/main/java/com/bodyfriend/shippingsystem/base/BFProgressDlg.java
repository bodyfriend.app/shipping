package com.bodyfriend.shippingsystem.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.bodyfriend.shippingsystem.R;

public class BFProgressDlg extends ProgressDialog {

	public BFProgressDlg(Context context, int theme) {
		super(context, theme);
	}

	public BFProgressDlg(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.c_progress);
		ImageView iv = (ImageView) findViewById(R.id.progressBar);
		((AnimationDrawable) iv.getDrawable()).start();
	}
}
