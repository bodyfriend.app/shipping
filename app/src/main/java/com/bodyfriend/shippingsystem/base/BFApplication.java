package com.bodyfriend.shippingsystem.base;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.base.common.BaseApplication;
import com.bodyfriend.shippingsystem.base.image.AUIL;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.NetRequest;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class BFApplication extends BaseApplication {


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();
        Auth.CREATE(context);
        AUIL.CREATE(context);
        PP.CREATE(context);
//        Typekit.getInstance()
//                .addNormal(Typekit.createFromAsset(this, BFFont.BODY_M))
//                .addBold(Typekit.createFromAsset(this, BFFont.BODY_B));

        // 테스트 URL일 경우 로그를 띄운다
        Log.setLog(BuildConfig.DEBUG);
        // 테스트 URL일 경우 로그를 띄운다.
        NetRequest.setLOG(BuildConfig.DEBUG);

        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
    }
}