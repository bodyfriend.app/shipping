package com.bodyfriend.shippingsystem.base.image;

import android.content.Context;
import android.support.annotation.DrawableRes;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class AUIL {
	public static void CREATE(Context context) {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)//
				.threadPriority(Thread.NORM_PRIORITY - 2)//
				.denyCacheImageMultipleSizesInMemory()//
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())//
				.diskCacheFileCount(100) //
				.tasksProcessingOrder(QueueProcessingType.LIFO)
//				.writeDebugLogs()//
				.build();
		ImageLoader.getInstance().init(config);
	}

	public static DisplayImageOptions options = new DisplayImageOptions.Builder()//
			.cacheInMemory(true)//
			.cacheOnDisk(true)//
			.considerExifParams(true)//
			.build();

//	.diskCacheFileCount(100) //

	public static DisplayImageOptions noDisk_options = new DisplayImageOptions.Builder()//
			.cacheInMemory(true)//
			.cacheOnDisk(false)//
			.considerExifParams(true)//
			.build();

	public static String drawable2uri(@DrawableRes int drawable) {
		return "drawable://" + drawable;
	}
	/**assets://image.png*/
	public static String assets2uri(String assets_image) {
		return "assets://" + assets_image;
	}
//	String imageUri = "assets://image.png"; // from assets
//	String imageUri = "drawable://" + R.drawable.image; // from drawables (only images, non-9patch)
//	{
//		  Class res = R.drawable.class;
//		  Field field = res.getField("resource_name");
//		  int drawableId = field.getInt(null);
//		  
//		  int drawableId = getResources().getIdentifier("resource_name", "drawable", "com.example.package");
//		}

}
