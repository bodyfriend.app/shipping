package com.bodyfriend.shippingsystem.base.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

/**
 * @author djrain
 * 
 *         <pre>
 * 기능 : ssp관련 파일IO Util
 *  
 * file     => xxxxxxxxxx.xxx              filename and ext
 * filename => xxxxxxxxxxx                 has no ext
 * pathfile => /xxxx/xxxxx/xxxxxxxxx.xxx   absolute path + filename + ext
 * ext      => .xxx                        note : has dot(".")
 * 
 * &lt;uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * </pre>
 */
public class FU {
	private static File ROOT;
	/** 파일네임에 적합하지 않은것 */
	private static final String regularExpressionVFilename = "\\\\/|[&{}?=/\\\\: <>*|\\]\\[\\\"\\']";

	public static void CREATE(Context context) {
		ROOT = context.getExternalFilesDir(null);
	}
	public static void CREATE(String rootFolder) {
		ROOT = makePath(Environment.getExternalStorageDirectory() + "/" + rootFolder);
	}

	public static long getFreeBytes() {
		return new android.os.StatFs(ROOT.getPath()).getFreeBytes();
	}
	public static File getRoot() {
		if (ROOT == null)
			throw new NullPointerException("u forget call? FU.CREATE(context)");
		return ROOT;
	}
	protected static File makePath(final Object path) {
		File fileDir = toFile(path);
		if (!fileDir.isDirectory())
			fileDir.mkdirs();
		return fileDir;
	}
	public static String uName() {
		return UUID.randomUUID().toString();
	}
	public static String vName(String name) {
		return name.replaceAll(regularExpressionVFilename, "_");
	}

	public static String getExt(Object file) {
		String filename = toString(file);
		return filename.substring(filename.lastIndexOf(".") + ".".length());
	}
	public static File getPathfile(String filename) {
		return toFile(getRoot() + "/" + filename);
	}

	public static long length(Object pathfile) {
		try {
			return toFile(pathfile).length();
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return -1;
	}

	public static boolean exists(Object pathfile) {
		try {
			return toFile(pathfile).exists();
		} catch (Exception e) {
//			e.printStackTrace();
		}
		return false;
	}

	public static boolean deleteR(Object pathfile) {
		File dir = toFile(pathfile);
		if (!dir.exists())
			return true;

		if (dir.isDirectory()) {
			String[] children = dir.list();
			if (children != null) {
				for (int i = 0; i < children.length; i++) {
					boolean success = deleteR(new File(dir, children[i]));
					if (!success) {
						return false;
					}
				}
			}
		}
		return dir.delete();
	}
	public static boolean delete(Object pathfile) {
		File dir = toFile(pathfile);
		if (!dir.exists())
			return true;

		File temp = getTempDir(dir.getName(), dir.getParentFile());
		dir.renameTo(temp);
		return deleteR(temp);
	}
	public static File toFile(Object file) {
		File f = null;
		if (file instanceof Uri) {
			f = new File(((Uri) file).getSchemeSpecificPart());
		} else if (file instanceof String) {
			f = new File((String) file);
		} else if (file instanceof File) {
			f = (File) file;
		} else {
			throw new UnsupportedOperationException("Must the file is Uri or String(pathfile) or File class");
		}
		return f;
	}
	public static String toString(Object file) {
		String filename = null;
		if (file instanceof Uri) {
			filename = ((Uri) file).getSchemeSpecificPart();
		} else if (file instanceof String) {
			filename = new File((String) file).getName();
		} else if (file instanceof File) {
			filename = ((File) file).getName();
		} else {
			throw new UnsupportedOperationException("Must the file is Uri or String(pathfile) or File class");
		}
		return filename;
	}
	public static Uri toUri(Object pathfile) {
		return Uri.fromFile(toFile(pathfile));
	}

	public static File getTempFile(String prefix, String suffix, File path) {
		try {
			return File.createTempFile(prefix, suffix, path);
		} catch (IOException e) {
//			e.printStackTrace();
		}
		return null;
	}
	public static File getTempFile(String prefix, String suffix) {
		return getTempFile(prefix, suffix, getRoot());
	}
	public static File getTempTime(String prefix, String suffix) {
		return getTempFile(prefix + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.getDefault()) + "_", suffix, getRoot());
	}
	public static File getTempDir(String prefix, File directory) {
		Random tempFileRandom = new Random();
		File result;
		do {
			result = new File(directory, prefix + tempFileRandom.nextInt());
		} while (result.exists());
		return result;
	}

	public static File[] getfiles(final String ext) {
		return getRoot().listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if (ext == null)
					return !pathname.isDirectory();
				else
					return !pathname.isDirectory() && pathname.getName().endsWith("." + ext);
			}
		});
	}

	@SuppressWarnings("resource")
	public static void copy(File source, File target) throws IOException {
		FileChannel inChannel = new FileInputStream(source).getChannel();
		FileChannel outChannel = new FileOutputStream(target, false).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

	public static void copy(String source, String target) throws IOException {
		copy(new File(source), new File(target));
	}

	public static void copyDB(SQLiteDatabase source, File target) throws IOException {
		copy(new File(source.getPath()), target);
	}
	public static void copy(InputStream is, OutputStream os) throws IOException {
		byte[] buffer = new byte[4096];
		int count = 0;
		while ((count = is.read(buffer)) >= 0) {
			os.write(buffer, 0, count);
		}
	}

//	try {
//		BufferedWriter buf = new BufferedWriter(new FileWriter(file));
//		buf.append(str);
//		buf.newLine();
//		buf.close();
//	} catch (IOException e) {
//	}
//
//	try {
//		BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), HTTP.UTF_8));
//		buf.append(str);
//		buf.newLine();
//		buf.close();
//	} catch (IOException e) {
//	}

	/**
	 * @param file
	 * @param obj
	 * <br>
	 *            not used just type<br>
	 *            must null
	 * @return T obj
	 * @throws IOException
	 * @throws StreamCorruptedException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings({"unchecked", "resource"})
	public static <T extends Serializable> T load(File file, T obj) throws StreamCorruptedException, IOException, ClassNotFoundException {
		if (obj != null)
			throw new IllegalArgumentException("obj must null");

//		Log.l(file, obj);
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		return (T) ois.readObject();
	}
	public static <T extends Serializable> boolean save(File file, T obj) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(obj);
		oos.close();
		fos.close();
		return true;
	}

	public static void saveByte(File file, byte[] buffer) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(buffer);
		fos.close();
	}

	public static byte[] loadByte(File file) throws StreamCorruptedException, IOException {
		byte[] buffer = null;
		if (file.exists())
			buffer = new byte[(int) file.length()];

		FileInputStream fos = new FileInputStream(file);
		fos.read(buffer);
		fos.close();
		return buffer;
	}
	public static void saveString(File file, String data) throws IOException {
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
		writer.write(data);
		writer.close();
	}
	public static String loadString(File file) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[4096];

		StringBuilder sb = new StringBuilder(1024);
		int byteCount;
		while ((byteCount = fis.read(buffer)) > 0)
			sb.append(new String(buffer, 0, byteCount));

		fis.close();
		return sb.toString();

	}
	public static String loadLine(File file, int lineNumber) throws IOException {
		LineNumberReader buf = new LineNumberReader(new FileReader(file));
		buf.setLineNumber(lineNumber);
		String str = buf.readLine();
		buf.close();
		return str;
	}
	public static void saveLineadd(File file, String str) throws IOException {
		PrintWriter buf = new PrintWriter(new FileWriter(file, true));
		buf.println(str);
		buf.close();
	}

	/**
	 * with pair loadUTF
	 * @see loadUTF
	 * @param file
	 * @param str
	 * @throws IOException
	 */
	public static void saveUTF(File file, String str) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeUTF(str);
		oos.close();
		fos.close();
	}

	/**
	 * with pair saveUTF
	 * @see saveUTF
	 * @param file
	 * @return
	 * @throws StreamCorruptedException
	 * @throws IOException
	 */
	public static String loadUTF(File file) throws StreamCorruptedException, IOException {
		FileInputStream fos = new FileInputStream(file);
		ObjectInputStream oos = new ObjectInputStream(fos);
		String value = oos.readUTF();
		oos.close();
		fos.close();
		return value;
	}

}
