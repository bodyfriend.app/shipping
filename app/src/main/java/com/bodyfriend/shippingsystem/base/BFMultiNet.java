package com.bodyfriend.shippingsystem.base;

import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.MultipartAsyncTask;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.main.login.Auth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by 이주영 on 2016-12-01.
 */

public class BFMultiNet extends MultipartAsyncTask {
    public interface OnNetResultListener {
        void onResult(boolean isSuccess, String resultMsg);
    }

    private OnNetResultListener onNetResultListener;

    public BFMultiNet setOnNetResultListener(OnNetResultListener onNetResultListener) {
        this.onNetResultListener = onNetResultListener;
        return this;
    }

    private BFMultiNet(String requestURL) {
        super(requestURL, "Cookie", String.format("JSESSIONID=%s", Auth.d.resultData.SID));
    }

    public static BFMultiNet newInstance(String requestURL) {
        return new BFMultiNet(requestURL);
    }

    public void execute() {
        super.execute();

        OH.c().notifyObservers(OH.TYPE.SHOW_PROGRESS);
    }

    public BFMultiNet setFilePart(Object... fileParts) {
        setFileParts(fileParts);
        return this;
    }

    public BFMultiNet setFormField(String... formField) {
        setFormFields(formField);
        Log.d(mRequestURL + " > " + formField.toString());
        return this;
    }

    @Override
    protected void onResult(List<String> resultList) {
        boolean isSuccess = false;
        String resultMsg = null;
        for (String str : resultList) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(str);
                isSuccess = jsonObject.getInt("resultCode") == 1;
                resultMsg = jsonObject.getString("resultMsg");
                if (isSuccess) break;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        OH.c().notifyObservers(OH.TYPE.HIDE_PROGRESS);
        this.onNetResultListener.onResult(isSuccess, resultMsg);
    }
}
