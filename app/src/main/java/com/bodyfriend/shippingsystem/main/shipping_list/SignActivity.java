package com.bodyfriend.shippingsystem.main.shipping_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.RequestCodes;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeProvision;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeRelative;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class SignActivity extends BFActivity {

    public static class RESULT {
        public final static String SCREEN_SIGN = "SCREEN_SIGN";
        /**
         * 관계코드 (기타 : 99)
         */
        public final static String M_RELATIONSHIP = "M_RELATIONSHIP";
        /**
         * 관계명
         */
        public final static String M_RELATIONSHIPNM = "M_RELATIONSHIPNM";
        /**
         * 비고
         */
        public final static String M_CONFIRMPS = "M_CONFIRMPS";
    }

    public static class EXTRA {
        public final static String PRODUCT_TYPE = "PRODUCT_TYPE";
    }

    private String productType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_activity);
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();

        productType = getIntent().getStringExtra(SignActivity.EXTRA.PRODUCT_TYPE);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setFinish(R.id.close);
        setOnClickListener(R.id.ok, onOkClickListener);

        showProgress();
        Net.async(new searchCodeRelative()).setOnNetResponse(onNetRelativeResponse);
        showProgress();
        Net.async(new searchCodeProvision()).setOnNetResponse(onNetProvisionResponse);

        setOnClickListener(R.id.sign, onSignClickListener);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
    }

    private List<searchCodeRelative.Data.resultData> mRelativeResultData;
    private Net.OnNetResponse<searchCodeRelative> onNetRelativeResponse = new Net.OnNetResponse<searchCodeRelative>() {

        private OnItemSelectedListener listener = new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position < mRelativeResultData.size()) {
                    setVisibility(R.id.m_relationshipnm, View.INVISIBLE);
                } else {
                    setVisibility(R.id.m_relationshipnm, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        @Override
        public void onResponse(searchCodeRelative response) {

            hideProgress();
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            mRelativeResultData = response.data.resultData;

            ArrayList<String> arrayList = new ArrayList<String>();

            for (searchCodeRelative.Data.resultData data : response.data.resultData) {
                arrayList.add(data.DET_CD_NM);
            }

            arrayList.add("기타");

            if (arrayList.size() == 1) {
                setVisibility(R.id.m_relationshipnm, true);
            }

            setSpinner(R.id.relativeSpinner, arrayList);
            Spinner spinner = (Spinner) findViewById(R.id.relativeSpinner);
            spinner.setOnItemSelectedListener(listener);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    private ArrayList<CheckBox> arrayList;
    private Net.OnNetResponse<searchCodeProvision> onNetProvisionResponse = new Net.OnNetResponse<searchCodeProvision>() {

        @Override
        public void onResponse(searchCodeProvision response) {
            hideProgress();
            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            arrayList = new ArrayList<CheckBox>();
            LinearLayout layout = (LinearLayout) findViewById(R.id.provisionLayout);
            for (searchCodeProvision.Data.resultData data : response.data.resultData) {
                if (data.DET_CD.startsWith(productType)) {
                    CheckBox checkBox = new CheckBox(mContext);
                    checkBox.setTag(data);
                    checkBox.setText(data.DESCRIPTION);
                    checkBox.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                    layout.addView(checkBox);
                    View view = new View(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
                    view.setLayoutParams(params);
                    view.setBackgroundColor(Color.BLACK);
                    layout.addView(view);

                    arrayList.add(checkBox);
                }
            }

            if (arrayList.size() != 0) {
                CheckBox checkBox = new CheckBox(mContext);
                checkBox.setText("전체 동의");
                checkBox.setOnCheckedChangeListener(onAllCheckedChangeListener);
                layout.addView(checkBox);
                arrayList.add(checkBox);
            }

        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    @Override
    public boolean check() {
        if (null != arrayList) {
            for (int i = 0; i < arrayList.size() - 1; i++) {
                CheckBox cb = arrayList.get(i);
                if (!cb.isChecked()) {
                    searchCodeProvision.Data.resultData data = (searchCodeProvision.Data.resultData) cb.getTag();
                    toast("%s 항목을 확인하여 주십시오.", data.DESCRIPTION);
                    return false;
                }
            }

            if (screen_sign == null) {
                toast("서명을 입력하여 주십시오.");
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    private OnClickListener onOkClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (!check()) {
                return;
            }

            Intent data = new Intent();
            final int position = getSelectedItemPosition(R.id.relativeSpinner);
            String m_relationshipnm;
            String m_relationship;
            if (mRelativeResultData.size() != position) {
                final searchCodeRelative.Data.resultData item = mRelativeResultData.get(position);
                m_relationshipnm = item.DET_CD_NM;
                m_relationship = item.DET_CD;
            } else {
                m_relationshipnm = text(R.id.m_relationshipnm); // 기타
                m_relationship = "99";
            }

            data.putExtra(RESULT.SCREEN_SIGN, screen_sign);
            data.putExtra(RESULT.M_RELATIONSHIPNM, m_relationshipnm);
            data.putExtra(RESULT.M_RELATIONSHIP, m_relationship);
            data.putExtra(RESULT.M_CONFIRMPS, text(R.id.m_confirmps)); // 비고
            setResult(RESULT_OK, data);
            finish();
        }
    };

    private OnClickListener onSignClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, FullSignActivity.class);
            startActivityForResult(intent, RequestCodes.FULLSIGN);

        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    private OnCheckedChangeListener onAllCheckedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            for (int i = 0; i < arrayList.size() - 1; i++) {
                arrayList.get(i).setChecked(isChecked);
            }

        }
    };
    private String screen_sign;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestCodes.FULLSIGN && resultCode == Activity.RESULT_OK) {
            screen_sign = data.getStringExtra(FullSignActivity.RESULT.SCREEN_SIGN);

            Bitmap bitmap = BitmapFactory.decodeFile(screen_sign.replace("file:", ""));
            final ImageView signView = (ImageView) findViewById(R.id.signView);
            signView.setImageBitmap(bitmap);
            setVisibility(R.id.signView, true);
        }
    }

    private File getSignFile() {
        URI uri = null;
        try {
            if (screen_sign != null)
                uri = new URI(screen_sign);
        } catch (URISyntaxException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        File sign = null;
        if (uri != null) {
            sign = new File(uri.getPath());
        }
        return sign;
    }
}
