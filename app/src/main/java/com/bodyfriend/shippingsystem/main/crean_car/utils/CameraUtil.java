package com.bodyfriend.shippingsystem.main.crean_car.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.bodyfriend.shippingsystem.base.util.SDF;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by 이주영 on 2016-12-14.
 */
public class CameraUtil {

    public interface OnCameraListener {
        void onCreatedImage(Bitmap bitmap, File file);

        void onFail(Exception e);
    }

    private OnCameraListener onCameraListener;

    private static CameraUtil ourInstance;
    private GalleryLoader mGalleryLoader;
    private Context mContext;
    private String mIamgeName;

    public synchronized static CameraUtil getInstance() {
        if (ourInstance == null) {
            ourInstance = new CameraUtil();
        }

        return ourInstance;
    }

    private CameraUtil() {
    }

    public void getImage(Activity activity, String imageName, OnCameraListener onCameraListener) {
        mContext = activity;
        mIamgeName = imageName;
        this.onCameraListener = onCameraListener;

        initGalleryLoader(activity);
        mGalleryLoader.getImage();
    }

    /**
     * 카메라를 시작한다
     *
     * @param activity
     * @param imageName
     * @param onCameraListener
     */
    public void startCamera(Activity activity, String imageName, OnCameraListener onCameraListener) {
        mContext = activity;
        mIamgeName = imageName;

        initGalleryLoader(activity);

        mGalleryLoader.startCamera();
        this.onCameraListener = onCameraListener;
    }

    private void initGalleryLoader(Activity activity) {
        mGalleryLoader = new GalleryLoader(activity);
        mGalleryLoader.setOnGalleryLoaderListener(onPhotoGalleryLoaderListener);
    }

    GalleryLoader.OnGalleryLoader onPhotoGalleryLoaderListener = new GalleryLoader.OnGalleryLoader() {

        private int bitmapHeight;
        private int bitmapWidth;

        @Override
        public void onLoaded(GalleryLoader galleryLoader, Uri data) {
            try {
                File pathfile = writeImage(mIamgeName);

                FileOutputStream os = new FileOutputStream(pathfile);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data);

                ExifInterface exif = new ExifInterface(data.getPath());
                int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int exifDegree = exifOrientationToDegrees(exifOrientation);

                final int maxSize = 640;

                try {
                    final int h = bitmap.getHeight();
                    if (h != 0) {
                        bitmapHeight = h;
                    }

                    final int w = bitmap.getHeight();
                    if (w != 0) {
                        bitmapWidth = bitmap.getWidth();
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }

                if (bitmapWidth == 0 && bitmapHeight == 0) {
                    bitmapHeight = 640;
                    bitmapWidth = 320;
                }

                float newHeight; // 1000
                float newWidth; // 500
                if (bitmapHeight > bitmapWidth) {
                    newHeight = maxSize;
                    newWidth = (newHeight / bitmapHeight * bitmapWidth);
                } else {
                    newWidth = maxSize;
                    newHeight = (newWidth / bitmapWidth * bitmapHeight);
                }

                bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
                bitmap = rotate(bitmap, exifDegree);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);

                os.close();

                onCameraListener.onCreatedImage(bitmap, pathfile);

            } catch (FileNotFoundException e) {
                onCameraListener.onFail(e);
            } catch (IOException e) {
                onCameraListener.onFail(e);
            }
        }
    };

    /**
     * 이미지를 회전시킵니다.
     *
     * @param bitmap  비트맵 이미지
     * @param degrees 회전 각도
     * @return 회전된 이미지
     */
    public Bitmap rotate(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != converted) {
                    bitmap.recycle();
                    bitmap = converted;
                }
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
                // 메모리가 부족하여 회전을 시키지 못할 경우 그냥 원본을 반환합니다.
            }
        }
        return bitmap;
    }

    /**
     * EXIF정보를 회전각도로 변환하는 메서드
     *
     * @param exifOrientation EXIF 회전각
     * @return 실제 각도
     */
    public int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private File writeImage(String fileName) {
        File pathfile = Environment.getExternalStorageDirectory();

        String date = SDF.yyyymmdd.format(System.currentTimeMillis());

        pathfile = new File(pathfile.getPath() + "/bodyfreind/service/" + date + "/" + mIamgeName);
        if (!pathfile.exists())
            pathfile.mkdirs();

        pathfile = new File(pathfile.getPath() + "/" + fileName);
        return pathfile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mGalleryLoader != null)
            mGalleryLoader.onActivityResult(requestCode, resultCode, data);
    }
}
