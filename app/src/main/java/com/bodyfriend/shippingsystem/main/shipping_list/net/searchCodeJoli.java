package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 조리수기
 */
public class searchCodeJoli extends searchCodeList {

	public searchCodeJoli() {
		super();
		setParam("code", 9602);

	}

	@Override
	protected void parse(String json) {
		super.parse(json);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}

	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "ResultData": [
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): {
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_DT": 1434688253073,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD": "O",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DESCRIPTION": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD_NM": "O",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD_NM": "조리수기",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_DT": 1434688253073,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "GROUP_CD": "SP",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "SORT_SEQ": "1",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_2": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_3": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_1": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD": "9602"
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): },
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): {
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_DT": 1434688259233,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD": "X",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DESCRIPTION": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD_NM": "X",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD_NM": "조리수기",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_DT": 1434688259233,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "GROUP_CD": "SP",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "SORT_SEQ": "2",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_2": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_3": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_1": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD": "9602"
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): },
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): {
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_DT": 1434688266017,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD": "Y",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DESCRIPTION": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "DET_CD_NM": "증정",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD_NM": "조리수기",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "UPD_DT": 1434688266017,
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "GROUP_CD": "SP",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "INS_ID": "adminsp",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "SORT_SEQ": "3",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_2": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_3": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "REF_1": "",
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): "COMM_CD": "9602"
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): }
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): ]
	// 06-19 14:52:02.116: W/_NETLOG_IN(657): }:at (NetRequest.java:202)

}
