package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.base.NetConst;

public class logout extends BFEnty {

	public logout() {
		url = NetConst.host + "login/appLogout.json";
	}
}
