package com.bodyfriend.shippingsystem.main.benefit;

import java.util.ArrayList;

public class BenefitData {

    public ArrayList<resultData> resultData;

    public class resultData {
        public String DELIVERYMAN;
        public int GRAND_TOTAL;
        public String TEL_NO;
        public String YEAR;
        public String DEPARTMENT_NM;
        public String EMAIL;
        public int MONTH;
    }

   /* "ResultData": [
    {
        "DELIVERYMAN": "devtest",
            "GRAND_TOTAL": 90000,
            "TEL_NO": "01027531845",
            "YEAR": 2018,
            "DEPARTMENT_NM": "개발팀",
            "DELIVERYMAN_NM": "개발테스트용",
            "EMAIL": "khlee@bodyfriend.co.kr",
            "MONTH": 4
    }
    ]*/
}
