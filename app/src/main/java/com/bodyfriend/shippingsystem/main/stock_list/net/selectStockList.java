package com.bodyfriend.shippingsystem.main.stock_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * Created by 이주영 on 2016-10-25.
 * <p>
 * 재고리스트 :  "/ShippingMgt/ShippingTeamMgt/selectStockList.json"
 * 파라미터 : whSeq='창고SEQ'&productType='제품타입'
 * (창고SEQ는 창고리스트를 받아서 세팅, 제품타입은 안마의자 : M, 라텍스 : L 고정)
 * 리턴값 : map
 * (
 * map.NUM : ROWNUM
 * , map.PRODUCT_NAME : 제품명
 * , map.WH_NAME 창고명
 * , map.REG_DATE : 엑셀업로드날짜
 * , map.QTY1 : 양품재고
 * , map.QTY2 : 리퍼재고
 * , map.QTY3 : 양품예약현황
 * , map.QTY4 : 리퍼 예약 현황
 * )
 * ------------------------------------------------------
 * 창고리스트(SELECT BOX 세팅) : "/stats/admin/searchWareHouseList.json"
 * 파라미터 : 없음
 * 리턴값 : map
 * (
 * map.WH_SEQ : 창고SEQ
 * , map.WH_NAME : 창고명
 * , map.AREA : 지역
 * )
 */

public class selectStockList extends BFEnty {

    /**
     * @param whSeq       창고SEQ
     * @param productType 제품타입
     */
    public selectStockList(String whSeq, String productType) {
        setUrl("ShippingMgt/ShippingTeamMgt/selectStockList.json");
        setParam("whSeq", whSeq, "productType", productType);
    }

    public Data data;

    public static class Data {
        public resultData resultData;

        public class resultData {
            public String listCount;
            public List<list> list;

            public class list {
                public String REG_DATE;
                public String PRODUCT_NAME;
                public String QTY6;
                public String WH_NAME;
                public String QTY2;
                public String QTY3;
                public String QTY4;
                public String QTY5;
                public String STOCK_SEQ;
                public String NUM;
                public String QTY1;
            }
        }
    }
}
