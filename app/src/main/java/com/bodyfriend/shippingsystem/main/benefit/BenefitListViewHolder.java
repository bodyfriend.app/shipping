package com.bodyfriend.shippingsystem.main.benefit;

import android.support.v7.widget.RecyclerView;

import com.bodyfriend.shippingsystem.databinding.ItemBenefitBinding;

class BenefitListViewHolder extends RecyclerView.ViewHolder {
    public final ItemBenefitBinding mBinding;

    public BenefitListViewHolder(ItemBenefitBinding itemView) {
        super(itemView.getRoot());
        mBinding = itemView;
    }
}
