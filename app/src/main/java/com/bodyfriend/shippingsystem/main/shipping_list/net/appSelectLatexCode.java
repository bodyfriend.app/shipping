package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * 사은품
 */
public class appSelectLatexCode extends BFEnty {

    public appSelectLatexCode() {
        setUrl("mobile/api/appSelectLatexCode.json");
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;

        public class resultData {
            public String PRODUCT_NAME;
            public String PRODUCT_CODE;
        }
    }
}
