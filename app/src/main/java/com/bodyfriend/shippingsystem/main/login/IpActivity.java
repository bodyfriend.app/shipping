package com.bodyfriend.shippingsystem.main.login;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.main.login.custom.BlurBehind;

public class IpActivity extends BFActivity implements CC.ILOGIN {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ip);

        BlurBehind.getInstance().withAlpha(80).withFilterColor(Color.BLACK).setBackground(this);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setOnClickListener(R.id.wow, onClickIpListener);

        String testIp = PP.SAMPLE_IP_1.get();
        if (testIp == null || testIp.isEmpty()) {
            PP.SAMPLE_IP_1.set(NetConst.HOST_TEST);
        }

        setText(R.id.currentIp, String.format("현재 설정된 아이피는 %s 입니다.", NetConst.host));
        setText(R.id.ipSample1, NetConst.HOST_REAL);
        setText(R.id.ipSample2, PP.SAMPLE_IP_1.get());
        setText(R.id.ipSample3, PP.SAMPLE_IP_2.get());

        setOnClickListener(R.id.ipSample1, onClickIpBtnListener);
        setOnClickListener(R.id.ipSample2, onClickIpBtnListener);
        setOnClickListener(R.id.ipSample3, onClickIpBtnListener);
    }

    private View.OnClickListener onClickIpBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setText(R.id.ip, ((TextView) v).getText());

        }
    };

    private View.OnClickListener onClickIpListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDialog("아이피가 정상적으로 변경되었습니다.", "확인", positiveListener);
        }

        DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                final String ip = text(R.id.ip);
                NetConst.host = ip;

                PP.CHANGED_IP.set(ip);
                PP.SAMPLE_IP_2.set(PP.SAMPLE_IP_1.get());
                PP.SAMPLE_IP_1.set(ip);
                finish();
            }
        };
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}