package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class ShippingMasterDetail extends BFEnty {

    public ShippingMasterDetail(String shipping_seq) {
        setUrl("mobile/api/appShippingMasterDetail.json");
        setParam("shipping_seq", shipping_seq, "deleteFlag", "N");
    }

    public Data data;

    public static class Data {
        public resultData resultData;
        public int resultCode;
        public String resultMsg;

        public class resultData {
            public String INS_COMPLETE_NM;
            public String PICTURE_PATH_THREE;
            public String TEL_NO2;
            public String FREEGIFT_ONE;
            public String INSADDR2;
            public String PURCHASING_OFFICE;
            public String imagesign;
            public String COLORTYPE;
            public String PICTURE_PATH_SIX;
            public String PRODUCT_SN;
            public String AREA2;
            public String imagefour;
            public String PICTURE_PATH_TWO;
            public String COMPANY_CHECK;
            public String MUST_FLAG;
            public String FREEGIFT_CHECK_TWO;
            public String PICTURE_PATH_FIVE;
            public String REFER_NAME;
            public String AGREE_NAME;
            public String CASH_RECEIPT_CARD_NUM;
            public String PROMISE;
            public String BACK_SN;
            public String WATER_COOKING;
            public String BACK_PICTURE_TWO;
            public String DUE_DATE;
            public String DELIVERYMAN1;
            public String SERVER_TIME;
            public String PROMISE_FAIL_CD_NM;
            public String SERVICE_MONTH;
            public String DELIVERYMAN3;
            public String DELIVERYMAN2;
            public String DIV_STATUS;
            public String backimageone;
            public String imagetwo;
            public String CUST_NAME;
            public String backimagethree;
            public String WATER_ADAPTOR;
            public String PRODUCT_TYPE;
            public String FREEGIFT_TWO;
            public String PROMISE_FAIL_PS;
            public String FAX_NO;
            public String imagethree;
            public String PICTURE_PATH_SIGN;
            public String BACK_PICTURE_ONE;
            public String FREEGIFT_CHECK_ONE;
            public String PRODUCT_NAME;
            public String PRODUCT_CODE;
            public String PROMISE_FAIL_CD;
            public String COST_AMOUNT;
            public String INS_COMPLETE;
            public String backimagetwo;
            public String PICTURE_PATH_ONE;
            public String HPHONE_NO2;
            public String BODY_NO;
            public String COSTNCOST;
            public String PAYMENT_TYPE;
            public String QTY;
            public String PRODUCT_TYPE_NM;
            public String HPHONE_NO;
            public String imagesix;
            public String imageone;
            public String SYMPTOM;
            public String PROMISE_TIME;
            public String DELIVERYMAN_NM2;
            public String AREA;
            public String BACK_PICTURE_THREE;
            public String DELIVERYMAN_NM3;
            public String PROGRESS_NO;
            public String DELIVERYMAN_NM1;
            public String TEL_NO;
            public String PICTURE_PATH_FOUR;
            public String SHIPPING_SEQ;
            public String SHIPPING_TYPE;
            public String BACK_DATE;
            public String INSADDR;
            public String imagefive;
            public String PROGRESS_NO_NM; //주소 이전 설치 여부 확인
            public String IN_DATE;
            public String DELYN;
            public String RECEIVE_DATE;
            public String CUST_UPDATE;

            public String SHIPPING_PS;
            public String CASH_RECEIPT_TYPE;
            public String L_FREEGIFT; // 선택한 라텍스 사은품 코드
            public String L_FREEGIFT_NM; // 선택한 라텍스 사은품 이름

            public boolean IS_LADDER; // 사다리차 사용여부
            public String LADDER_PRICE; // 사다리차 금액
            public String ladder_image_one; // 사다리차 사진1
            public String ladder_image_two; // 사다리차 사진2
            public String ladder_image_three; //사다리차 사진3
            public int LADDER_PAYMENT_TYPE; // 페이먼트 타입

            public boolean free_gift_warn; // 사은품 여부

            public boolean IS_SOCKET; // 여부
            public String socket_image_one; // 양중1
            public String socket_image_two; // 양중 2
            public String socket_image_three; // 양중 3
            public String ADDITIONAL_LADDER_PRICE; // 사다리 추가 요금
            public String m_extra_cost_memo; // 사다리 비용 비고
            public String FREEGIFT_ADDR; // 사은품 주소
//            public String SYMPTOM; // 증상



            public String service_image_one; // 내림서비스1
            public String service_image_two; // 내림서비스2

            @Override
            public String toString() {
                return "ResultData{" +
                        "INS_COMPLETE_NM='" + INS_COMPLETE_NM + '\'' +
                        ", PICTURE_PATH_THREE='" + PICTURE_PATH_THREE + '\'' +
                        ", TEL_NO2='" + TEL_NO2 + '\'' +
                        ", FREEGIFT_ONE='" + FREEGIFT_ONE + '\'' +
                        ", INSADDR2='" + INSADDR2 + '\'' +
                        ", PURCHASING_OFFICE='" + PURCHASING_OFFICE + '\'' +
                        ", imagesign='" + imagesign + '\'' +
                        ", COLORTYPE='" + COLORTYPE + '\'' +
                        ", PICTURE_PATH_SIX='" + PICTURE_PATH_SIX + '\'' +
                        ", PRODUCT_SN='" + PRODUCT_SN + '\'' +
                        ", AREA2='" + AREA2 + '\'' +
                        ", imagefour='" + imagefour + '\'' +
                        ", PICTURE_PATH_TWO='" + PICTURE_PATH_TWO + '\'' +
                        ", COMPANY_CHECK='" + COMPANY_CHECK + '\'' +
                        ", MUST_FLAG='" + MUST_FLAG + '\'' +
                        ", FREEGIFT_CHECK_TWO='" + FREEGIFT_CHECK_TWO + '\'' +
                        ", PICTURE_PATH_FIVE='" + PICTURE_PATH_FIVE + '\'' +
                        ", REFER_NAME='" + REFER_NAME + '\'' +
                        ", AGREE_NAME='" + AGREE_NAME + '\'' +
                        ", CASH_RECEIPT_CARD_NUM='" + CASH_RECEIPT_CARD_NUM + '\'' +
                        ", PROMISE='" + PROMISE + '\'' +
                        ", BACK_SN='" + BACK_SN + '\'' +
                        ", WATER_COOKING='" + WATER_COOKING + '\'' +
                        ", BACK_PICTURE_TWO='" + BACK_PICTURE_TWO + '\'' +
                        ", DUE_DATE='" + DUE_DATE + '\'' +
                        ", DELIVERYMAN1='" + DELIVERYMAN1 + '\'' +
                        ", SERVER_TIME='" + SERVER_TIME + '\'' +
                        ", PROMISE_FAIL_CD_NM='" + PROMISE_FAIL_CD_NM + '\'' +
                        ", SERVICE_MONTH='" + SERVICE_MONTH + '\'' +
                        ", DELIVERYMAN3='" + DELIVERYMAN3 + '\'' +
                        ", DELIVERYMAN2='" + DELIVERYMAN2 + '\'' +
                        ", DIV_STATUS='" + DIV_STATUS + '\'' +
                        ", backimageone='" + backimageone + '\'' +
                        ", imagetwo='" + imagetwo + '\'' +
                        ", CUST_NAME='" + CUST_NAME + '\'' +
                        ", backimagethree='" + backimagethree + '\'' +
                        ", WATER_ADAPTOR='" + WATER_ADAPTOR + '\'' +
                        ", PRODUCT_TYPE='" + PRODUCT_TYPE + '\'' +
                        ", FREEGIFT_TWO='" + FREEGIFT_TWO + '\'' +
                        ", PROMISE_FAIL_PS='" + PROMISE_FAIL_PS + '\'' +
                        ", FAX_NO='" + FAX_NO + '\'' +
                        ", imagethree='" + imagethree + '\'' +
                        ", PICTURE_PATH_SIGN='" + PICTURE_PATH_SIGN + '\'' +
                        ", BACK_PICTURE_ONE='" + BACK_PICTURE_ONE + '\'' +
                        ", FREEGIFT_CHECK_ONE='" + FREEGIFT_CHECK_ONE + '\'' +
                        ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                        ", PRODUCT_CODE='" + PRODUCT_CODE + '\'' +
                        ", PROMISE_FAIL_CD='" + PROMISE_FAIL_CD + '\'' +
                        ", COST_AMOUNT='" + COST_AMOUNT + '\'' +
                        ", INS_COMPLETE='" + INS_COMPLETE + '\'' +
                        ", backimagetwo='" + backimagetwo + '\'' +
                        ", PICTURE_PATH_ONE='" + PICTURE_PATH_ONE + '\'' +
                        ", HPHONE_NO2='" + HPHONE_NO2 + '\'' +
                        ", BODY_NO='" + BODY_NO + '\'' +
                        ", COSTNCOST='" + COSTNCOST + '\'' +
                        ", PAYMENT_TYPE='" + PAYMENT_TYPE + '\'' +
                        ", QTY='" + QTY + '\'' +
                        ", PRODUCT_TYPE_NM='" + PRODUCT_TYPE_NM + '\'' +
                        ", HPHONE_NO='" + HPHONE_NO + '\'' +
                        ", imagesix='" + imagesix + '\'' +
                        ", imageone='" + imageone + '\'' +
                        ", SYMPTOM='" + SYMPTOM + '\'' +
                        ", PROMISE_TIME='" + PROMISE_TIME + '\'' +
                        ", DELIVERYMAN_NM2='" + DELIVERYMAN_NM2 + '\'' +
                        ", AREA='" + AREA + '\'' +
                        ", BACK_PICTURE_THREE='" + BACK_PICTURE_THREE + '\'' +
                        ", DELIVERYMAN_NM3='" + DELIVERYMAN_NM3 + '\'' +
                        ", PROGRESS_NO='" + PROGRESS_NO + '\'' +
                        ", DELIVERYMAN_NM1='" + DELIVERYMAN_NM1 + '\'' +
                        ", TEL_NO='" + TEL_NO + '\'' +
                        ", PICTURE_PATH_FOUR='" + PICTURE_PATH_FOUR + '\'' +
                        ", SHIPPING_SEQ='" + SHIPPING_SEQ + '\'' +
                        ", SHIPPING_TYPE='" + SHIPPING_TYPE + '\'' +
                        ", BACK_DATE='" + BACK_DATE + '\'' +
                        ", INSADDR='" + INSADDR + '\'' +
                        ", imagefive='" + imagefive + '\'' +
                        ", PROGRESS_NO_NM='" + PROGRESS_NO_NM + '\'' +
                        ", IN_DATE='" + IN_DATE + '\'' +
                        ", DELYN='" + DELYN + '\'' +
                        ", RECEIVE_DATE='" + RECEIVE_DATE + '\'' +
                        ", CUST_UPDATE='" + CUST_UPDATE + '\'' +
                        ", SHIPPING_PS='" + SHIPPING_PS + '\'' +
                        ", CASH_RECEIPT_TYPE='" + CASH_RECEIPT_TYPE + '\'' +
                        ", L_FREEGIFT='" + L_FREEGIFT + '\'' +
                        ", L_FREEGIFT_NM='" + L_FREEGIFT_NM + '\'' +
                        ", IS_LADDER=" + IS_LADDER +
                        ", LADDER_PRICE='" + LADDER_PRICE + '\'' +
                        ", ladder_image_one='" + ladder_image_one + '\'' +
                        ", ladder_image_two='" + ladder_image_two + '\'' +
                        ", ladder_image_three='" + ladder_image_three + '\'' +
                        ", LADDER_PAYMENT_TYPE=" + LADDER_PAYMENT_TYPE +
                        ", free_gift_warn=" + free_gift_warn +
                        ", IS_SOCKET=" + IS_SOCKET +
                        ", socket_image_one='" + socket_image_one + '\'' +
                        ", socket_image_two='" + socket_image_two + '\'' +
                        ", socket_image_three='" + socket_image_three + '\'' +
                        ", ADDITIONAL_LADDER_PRICE='" + ADDITIONAL_LADDER_PRICE + '\'' +
                        ", m_extra_cost_memo='" + m_extra_cost_memo + '\'' +
                        ", service_image_one='" + service_image_one + '\'' +
                        ", service_image_two='" + service_image_two + '\'' +
                        '}';
            }
        }
    }
//
//    "ResultData": {
//        "INS_COMPLETE_NM": "",
//                "PICTURE_PATH_THREE": "",
//                "TEL_NO2": "",
//                "FREEGIFT_ONE": "",
//                "INSADDR2": "",
//                "PURCHASING_OFFICE": "",
//                "imagesign": "",
//                "COLORTYPE": "",
//                "PICTURE_PATH_SIX": "",
//                "PRODUCT_SN": "1111",
//                "AREA2": "   ",
//                "imagefour": "",
//                "PICTURE_PATH_TWO": "",
//                "COMPANY_CHECK": "",
//                "MUST_FLAG": "N",
//                "FREEGIFT_CHECK_TWO": "",
//                "PICTURE_PATH_FIVE": "",
//                "REFER_NAME": "",
//                "AGREE_NAME": "",
//                "CASH_RECEIPT_CARD_NUM": "",
//                "PROMISE": " ",
//                "BACK_SN": "",
//                "WATER_COOKING": "",
//                "BACK_PICTURE_TWO": "",
//                "DUE_DATE": "2016-12-19",
//                "DELIVERYMAN1": "mrjin3",
//                "SERVER_TIME": "20170102",
//                "PROMISE_FAIL_CD_NM": "",
//                "SERVICE_MONTH": "11",
//                "DELIVERYMAN3": "",
//                "DELIVERYMAN2": "dman2",
//                "DIV_STATUS": "I",
//                "backimageone": "",
//                "imagetwo": "",
//                "CUST_NAME": "11",
//                "backimagethree": "",
//                "WATER_ADAPTOR": "",
//                "PRODUCT_TYPE": "M",
//                "FREEGIFT_TWO": "",
//                "PROMISE_FAIL_PS": "",
//                "FAX_NO": "",
//                "imagethree": "",
//                "PICTURE_PATH_SIGN": "",
//                "BACK_PICTURE:at (NetRequest.java:220)
//        W: >>,들와:,_ONE": "",
//        "FREEGIFT_CHECK_ONE": "",
//                "PRODUCT_NAME": " 아이로보(아이보리) ",
//                "PRODUCT_CODE": "",
//                "PROMISE_FAIL_CD": "",
//                "COST_AMOUNT": "",
//                "INS_COMPLETE": "",
//                "backimagetwo": "",
//                "PICTURE_PATH_ONE": "",
//                "HPHONE_NO2": "",
//                "BODY_NO": "조립",
//                "COSTNCOST": "2",
//                "PAYMENT_TYPE": " ",
//                "QTY": "1",
//                "PRODUCT_TYPE_NM": "안마의자",
//                "HPHONE_NO": "11",
//                "imagesix": "",
//                "imageone": "",
//                "SYMPTOM": "",
//                "PROMISE_TIME": "",
//                "DELIVERYMAN_NM2": "박윤호",
//                "AREA": "   ",
//                "BACK_PICTURE_THREE": "",
//                "DELIVERYMAN_NM3": "",
//                "PROGRESS_NO": " ",
//                "DELIVERYMAN_NM1": "진철규",
//                "TEL_NO": "11",
//                "PICTURE_PATH_FOUR": "",
//                "SHIPPING_SEQ": "20161219M00007",
//                "SHIPPING_TYPE": "D70",
//                "BACK_DATE": "",
//                "INSADDR": "11",
//                "imagefive": "",
//                "IN_DATE": "",
//                "DELYN": "N",
//                "RECEIVE_DATE": "2016-12-19",
//                "SHIPPING_PS": "",
//                "CASH_RECEIPT_TYPE": " "

}
