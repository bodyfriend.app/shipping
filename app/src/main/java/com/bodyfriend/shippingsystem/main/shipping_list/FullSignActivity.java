package com.bodyfriend.shippingsystem.main.shipping_list;

import image.SignView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.util.FU;

public class FullSignActivity extends BFActivity {

    private static String customer_name = "";

    public static void setCustomerName(String customerName) {
        customer_name = customerName;
    }

    public static class RESULT {
        public final static String SCREEN_SIGN = "SCREEN_SIGN";
    }

    public static class EXTRA {
        public final static String PRODUCT_TYPE = "PRODUCT_TYPE";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_sign_activity);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        setOnClickListener(R.id.ok, onOkClickListener);
        setOnClickListener(R.id.cancel, onCancelClickListener);

        mSignView = (SignView) findViewById(R.id.signView);
        mSignView.setOnTouchListener(onSignTouchListener);
        setOnClickListener(R.id.clear, onClearClickListener);

        if (customer_name != null) {
            setText(R.id.cust_name, String.format("서명(%s 고객님)", customer_name));
        }
    }

    @Override
    protected void onLoad() {
        super.onLoad();
    }

    private SignView mSignView;
    private OnClickListener onClearClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            mSignView.clearCanvas();
            isTouchedSign = false;
        }
    };

    boolean isTouchedSign = false;
    private OnTouchListener onSignTouchListener = new OnTouchListener() {

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_MOVE:
                    isTouchedSign = true;
                    break;

                default:
                    break;
            }
            // TODO Auto-generated method stub
            return false;
        }
    };

    @Override
    public boolean check() {

        if (isTouchedSign == false) {
            toast("서명을 입력하여 주십시오.");
            return false;
        }

        return true;
    }

    ;

    private OnClickListener onCancelClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            setResult(RESULT_CANCELED);
            finish();
        }
    };

    private OnClickListener onOkClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (check() == false) {
                return;
            }

            Intent data = new Intent();
            data.putExtra(RESULT.SCREEN_SIGN, getSignUrl());
            setResult(RESULT_OK, data);
            finish();
        }
    };

    private String getSignUrl() {
        File pathfile = null;
        try {

            // File result;
            // do {
            // pathfile = new File(FU.getRoot(), "sign.png");
            // } while (!pathfile.createNewFile());

            pathfile = new File(FU.getRoot(), "sign.png");
            if (pathfile.isFile()) {
                pathfile.delete();
            }
            pathfile.createNewFile();

            // pathfile = FU.getTempFile("photo", ".png", FU.getRoot());
            FileOutputStream os = new FileOutputStream(pathfile);
            Bitmap bm = mSignView.getBitmap();
            final int newWidth = 400;
            final int newHeight = 200;
            bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
            bm.compress(CompressFormat.PNG, 100, os);
            os.close();

            // Log.d("저장위치는 >>> ", pathfile.toURI().toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathfile.toURI().toString();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
