package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class modifyDeliveryMan2 extends BFEnty {

    /**
     * @param m_deliveryman2
     * @param m_duedate
     * @param m_shippingseq
     */
    public modifyDeliveryMan2(String m_deliveryman2, String m_duedate, String m_shippingseq) {

        setUrl("mobile/api/modifyDeliveryMan2.json");

        setParam("m_deliveryman2", m_deliveryman2, "m_duedate", m_duedate, "m_shippingseq", m_shippingseq);
    }

    public Data data;

    public modifyDeliveryMan2(String m_deliveryman2, String m_duedate, String m_shippingseq, int mDelivryManIndex) {
        setUrl("mobile/api/modifyDeliveryMan2.json");
        if (mDelivryManIndex == 2) {
            setParam("m_deliveryman2", m_deliveryman2, "m_duedate", m_duedate, "m_shippingseq", m_shippingseq);
        } else {
            setParam("m_deliveryman3", m_deliveryman2, "m_duedate", m_duedate, "m_shippingseq", m_shippingseq);
        }
    }

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }

}
