package com.bodyfriend.shippingsystem.main.delivery_order.net;

import java.util.ArrayList;

/**
 * Created by Taeseok on 2018-03-05.
 */

public class ProductItemListData {
    public ArrayList<list> list;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductItemListData)) return false;

        ProductItemListData that = (ProductItemListData) o;

        return list != null ? list.equals(that.list) : that.list == null;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }

    public static class list {
        public String clsExpln;
        public String mfNatCd;
        public String mfNatNm;
        public String clsCd;
        public String itemId;
        public String clrtnNm;
        public String tagItemCd;
        public String itemNmKr;
        public String clrtnCd;
        public String itemNmEn;
        public String tagPreCtnt;
        public String itemExpln;
        public String partNm;
        public String clsNmKr;
        public String partCd;
        public int count;
        public String gdsStatCd;
        public String gdsLvlCd;

        public list(list result) {
            clsExpln = result.clsExpln;
            mfNatCd = result.mfNatCd;
            mfNatNm = result.mfNatNm;
            clsCd = result.clsCd;
            itemId = result.itemId;
            clrtnNm = result.clrtnNm;
            tagItemCd = result.tagItemCd;
            itemNmKr = result.itemNmKr;
            clrtnCd = result.clrtnCd;
            itemNmEn = result.itemNmEn;
            tagPreCtnt = result.tagPreCtnt;
            itemExpln = result.itemExpln;
            partNm = result.partNm;
            clsNmKr = result.clsNmKr;
            partCd = result.partCd;
            count = result.count;
            gdsStatCd = result.gdsStatCd;
            gdsLvlCd = result.gdsLvlCd;
        }
              /*  "clsExpln": "의자형 안마기",
                        "mfNatCd": "1",
                        "mfNatNm": "중국",
                        "clsCd": "C",
                        "itemId": "WH002017000001",
                        "clrtnNm": "브라운",
                        "tagItemCd": "NR",
                        "itemNmKr": "뉴레지나",
                        "clrtnCd": "BRN",
                        "itemNmEn": "New Regina",
                        "tagPreCtnt": "CNRBRNB1",
                        "itemExpln": "뉴레지나, 브라운, 중국, 바디, New Regina",
                        "partNm": "바디",
                        "clsNmKr": "안마의자",
                        "partCd": "CB"*/

        @Override
        public String toString() {
            return "list{" +
                    "clsExpln='" + clsExpln + '\'' +
                    ", mfNatCd='" + mfNatCd + '\'' +
                    ", mfNatNm='" + mfNatNm + '\'' +
                    ", clsCd='" + clsCd + '\'' +
                    ", itemId='" + itemId + '\'' +
                    ", clrtnNm='" + clrtnNm + '\'' +
                    ", tagItemCd='" + tagItemCd + '\'' +
                    ", itemNmKr='" + itemNmKr + '\'' +
                    ", clrtnCd='" + clrtnCd + '\'' +
                    ", itemNmEn='" + itemNmEn + '\'' +
                    ", tagPreCtnt='" + tagPreCtnt + '\'' +
                    ", itemExpln='" + itemExpln + '\'' +
                    ", partNm='" + partNm + '\'' +
                    ", clsNmKr='" + clsNmKr + '\'' +
                    ", partCd='" + partCd + '\'' +
                    ", count=" + count +
                    ", gdsStatCd='" + gdsStatCd + '\'' +
                    ", gdsLvlCd='" + gdsLvlCd + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof list)) return false;

            list list = (list) o;

            if (count != list.count) return false;
            if (clsExpln != null ? !clsExpln.equals(list.clsExpln) : list.clsExpln != null)
                return false;
            if (mfNatCd != null ? !mfNatCd.equals(list.mfNatCd) : list.mfNatCd != null)
                return false;
            if (mfNatNm != null ? !mfNatNm.equals(list.mfNatNm) : list.mfNatNm != null)
                return false;
            if (clsCd != null ? !clsCd.equals(list.clsCd) : list.clsCd != null) return false;
            if (itemId != null ? !itemId.equals(list.itemId) : list.itemId != null) return false;
            if (clrtnNm != null ? !clrtnNm.equals(list.clrtnNm) : list.clrtnNm != null)
                return false;
            if (tagItemCd != null ? !tagItemCd.equals(list.tagItemCd) : list.tagItemCd != null)
                return false;
            if (itemNmKr != null ? !itemNmKr.equals(list.itemNmKr) : list.itemNmKr != null)
                return false;
            if (clrtnCd != null ? !clrtnCd.equals(list.clrtnCd) : list.clrtnCd != null)
                return false;
            if (itemNmEn != null ? !itemNmEn.equals(list.itemNmEn) : list.itemNmEn != null)
                return false;
            if (tagPreCtnt != null ? !tagPreCtnt.equals(list.tagPreCtnt) : list.tagPreCtnt != null)
                return false;
            if (itemExpln != null ? !itemExpln.equals(list.itemExpln) : list.itemExpln != null)
                return false;
            if (partNm != null ? !partNm.equals(list.partNm) : list.partNm != null) return false;
            if (clsNmKr != null ? !clsNmKr.equals(list.clsNmKr) : list.clsNmKr != null)
                return false;
            if (partCd != null ? !partCd.equals(list.partCd) : list.partCd != null) return false;
            if (gdsStatCd != null ? !gdsStatCd.equals(list.gdsStatCd) : list.gdsStatCd != null)
                return false;
            return gdsLvlCd != null ? gdsLvlCd.equals(list.gdsLvlCd) : list.gdsLvlCd == null;
        }

        @Override
        public int hashCode() {
            int result = clsExpln != null ? clsExpln.hashCode() : 0;
            result = 31 * result + (mfNatCd != null ? mfNatCd.hashCode() : 0);
            result = 31 * result + (mfNatNm != null ? mfNatNm.hashCode() : 0);
            result = 31 * result + (clsCd != null ? clsCd.hashCode() : 0);
            result = 31 * result + (itemId != null ? itemId.hashCode() : 0);
            result = 31 * result + (clrtnNm != null ? clrtnNm.hashCode() : 0);
            result = 31 * result + (tagItemCd != null ? tagItemCd.hashCode() : 0);
            result = 31 * result + (itemNmKr != null ? itemNmKr.hashCode() : 0);
            result = 31 * result + (clrtnCd != null ? clrtnCd.hashCode() : 0);
            result = 31 * result + (itemNmEn != null ? itemNmEn.hashCode() : 0);
            result = 31 * result + (tagPreCtnt != null ? tagPreCtnt.hashCode() : 0);
            result = 31 * result + (itemExpln != null ? itemExpln.hashCode() : 0);
            result = 31 * result + (partNm != null ? partNm.hashCode() : 0);
            result = 31 * result + (clsNmKr != null ? clsNmKr.hashCode() : 0);
            result = 31 * result + (partCd != null ? partCd.hashCode() : 0);
            result = 31 * result + count;
            result = 31 * result + (gdsStatCd != null ? gdsStatCd.hashCode() : 0);
            result = 31 * result + (gdsLvlCd != null ? gdsLvlCd.hashCode() : 0);
            return result;
        }
    }
}
