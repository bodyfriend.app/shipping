package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by Taeseok on 2018-03-15.
 */

public class ModifyAddressConfirm extends BFEnty {

    /**
     * 변경 주소 확인
     *
     * @param m_shippingseq 배송 번호
     */
    public ModifyAddressConfirm(String m_shippingseq) {

        setUrl("mobile/api/ModifyAddressConfirm.json");

        setParam("m_shippingseq", m_shippingseq);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }
}
