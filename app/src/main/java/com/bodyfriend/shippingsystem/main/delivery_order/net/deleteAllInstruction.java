package com.bodyfriend.shippingsystem.main.delivery_order.net;


import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.main.login.Auth;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2016-12-29.
 */

public class deleteAllInstruction extends BFEnty {
    public deleteAllInstruction(ArrayList<createInstructionItem> items) {
        setUrl("common/warehouse/deleteinstruction.json ");
        setParam("deliveryManId", Auth.d.resultData.ADMIN_ID.trim());
        setParam("deliveryManName", Auth.d.resultData.ADMIN_NM.trim());

        ArrayList<String> instructionKey = new ArrayList<>();
        ArrayList<String> shippingSeq = new ArrayList<>();

        for (createInstructionItem item : items) {
            instructionKey.add(item.instructionKey);
            shippingSeq.add(item.shippingSeq);
        }

        setParam("shippingSeq", shippingSeq);
        setParam("instructionKey", instructionKey);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public resultData resultData;

        public class resultData {
            public String cause;

        }
    }
}
