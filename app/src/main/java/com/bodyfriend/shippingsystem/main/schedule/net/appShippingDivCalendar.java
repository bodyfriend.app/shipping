package com.bodyfriend.shippingsystem.main.schedule.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * 달력에서 요청하는 전문
 */
public class appShippingDivCalendar extends BFEnty {

    public appShippingDivCalendar(String dateStart, String dateEnd, String view_complete) {
        setUrl("mobile/api/appShippingDivCalendar.json");
        setParam("dateStart", dateStart, "dateEnd", dateEnd, "view_complete", view_complete);
    }

    public Data data;

    public static class Data {
        public resultData resultData;
        public int resultCode;
        public String resultMsg;

        public class resultData {
            public List<list> list;

            public class list {
                public String DUE_DATE;
                public int TOTAL;
                public int AD_CNT;
                public int D10CNT; // 배송
                public int D20CNT; // 맞교체
                public int D30CNT; // 계철
                public int P10CNT; // 이전
                public int P20CNT; // 수리
            }
        }
    }
}
