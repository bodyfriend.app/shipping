package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * 영업소
 */
public class selectDeliveryManList extends BFEnty {
    public selectDeliveryManList(String code) {
        showLog(false);
        setUrl("mobile/api/appSelectDeliveryManList.json");
        setParam("code", code);
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;
        public String resultMsg;
        public int resultCode;

        public class resultData {
            public String ADMIN_NM;
            public String ADMIN_ID;
        }
    }

//	05-04 11:32:47.972: W/_NETLOG_IN(4848):     "ResultData": [
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         {
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_NM": "서울영업소",
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_ID": "dcom1"
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         },
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         {
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_NM": "경기영업소",
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_ID": "dcom2"
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         },
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         {
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_NM": "충남영업소",
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_ID": "dcom3"
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         },
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         {
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_NM": "sptest2",
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):             "ADMIN_ID": "sptest2"
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):         }
//	05-04 11:32:47.972: W/_NETLOG_IN(4848):     ]

}
