package com.bodyfriend.shippingsystem.main.delivery_order.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.main.delivery_order.net.createInstruction;

import java.util.List;

/**
 * Created by 이주영 on 2017-01-03.
 */

public class WhFailAdapter extends PagerAdapter {

    private LayoutInflater mInflater;

    private List<createInstruction.Data.resultData> resultData;

    public WhFailAdapter(Context c, List<createInstruction.Data.resultData> resultData) {
        super();
        mInflater = LayoutInflater.from(c);
        this.resultData = resultData;
    }

    @Override
    public int getCount() {
        return resultData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View v = mInflater.inflate(R.layout.delvery_wh_fail_item, null);
        container.addView(v);

        createInstruction.Data.resultData data = resultData.get(position);

        ViewHoler viewHoler = (ViewHoler) v.getTag();
        if (viewHoler == null) {
            viewHoler = new ViewHoler(v);
            v.setTag(viewHoler);
        }

        viewHoler.setData(data);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View pager, Object obj) {
        return pager == obj;
    }

    class ViewHoler {
        TextView whsCd;
        TextView whsNm;
        TextView hrNm;
        TextView hrMbPhn;

        ViewHoler(View contentView) {
            whsCd = (TextView) contentView.findViewById(R.id.whsCd);
            whsNm = (TextView) contentView.findViewById(R.id.whsNm);
            hrNm = (TextView) contentView.findViewById(R.id.hrNm);
            hrMbPhn = (TextView) contentView.findViewById(R.id.hrMbPhn);
        }

        public void setData(createInstruction.Data.resultData data) {
//            whsCd.setText(data.whsCd); // 창고번호
//            whsNm.setText(data.whsNm); // 창고이름
//            hrNm.setText(data.hrNm); // 창고장이름
//            hrMbPhn.setText(data.hrMbPhn); // 창고장 전화번호
        }
    }
}
