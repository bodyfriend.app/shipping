package com.bodyfriend.shippingsystem.main.product.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class appProductDetail extends BFEnty {

	public appProductDetail(String productcode) {
		setUrl("mobile/api/appProductDetail.json");
		setParam("productcode", productcode);
	}

	public Data data;

	public static class Data {
		public resultData resultData;
		public String resultMsg;
		public int resultCode;

		public class resultData {
			public String PRODUCT_IMAGE_S;
			public String PRODUCT_TYPE;
			public String PRODUCT_IMAGE_L;
			public String PRODUCT_NAME;
			public String PRODUCT_CAUTION;
			public String PRODUCT_CODE;
			public String PRODUCT_SPEC;
			public String PRODUCT_DETAIL;
			public String imageone;
			public String imagetwo;
			public String PRODUCT_INTRO;
			
			public String PRODUCT_CAUTION1_MOV;
			public String PRODUCT_CAUTION2_MOV;
			public String PRODUCT_CAUTION3_MOV;
			public String PRODUCT_CAUTION4_MOV;
			public String PRODUCT_CAUTION5_MOV;
			
			public String PRODUCT_CAUTION1;
			public String PRODUCT_CAUTION2;
			public String PRODUCT_CAUTION3;
			public String PRODUCT_CAUTION4;
			public String PRODUCT_CAUTION5;
		}
	}
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "imageone": "\/file\/product\/20150605144342531239759.jpg",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_INTRO": "",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_IMAGE_S": "\/svc\/file\/product\/20150605144342531239759.jpg",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_IMAGE_L": "\/svc\/file\/product\/20150605144342533534135.jpg",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_NAME": "아이로보(그레이)",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION5_MOV": "https:\/\/www.youtube.com\/watch?v=7mMacw-en0s",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_SPEC": "눞혔을때 : 193x78x76\r\n세웠을때 : 137x78x120\r\n제품무게 : 79Kg\r\n외피 : 인조가죽",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CODE": "0000000004",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION4_MOV": "https:\/\/www.youtube.com\/watch?v=GK_gm7FD1cc",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION3_MOV": "https:\/\/www.youtube.com\/watch?v=zvPQQ1EENx0",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "imagetwo": "\/file\/product\/20150605144342533534135.jpg",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION1": "가장 먼저 주의하실점은 블라블라블라블라....",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION2": "이 제품은 설치시 이부분을 주의하셔야합니다... 참쉽죠?",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_TYPE": "M",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION1_MOV": "https:\/\/www.youtube.com\/watch?v=d6UpEFcQs6s",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_DETAIL": "자동체형인식\/손,팔안마\/온열히팅기능\/한글액정리모컨\/수면안마\/스트레칭기능\/다리길이조절\/자동리크라이닝",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION2_MOV": "https:\/\/www.youtube.com\/watch?v=1UXG7eytwAQ",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION3": "이 제품 설치시 여기를 놓치는 경우도 있으니 참고하시구요...",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION4": "설치 후 영상처럼 테스트를 해보시고.. 하판쪽 체크를 꼭 하셔야합니다.",
//	06-05 17:26:10.901: W/_NETLOG_IN(12286):         "PRODUCT_CAUTION5": "참쉽죠?"

}
