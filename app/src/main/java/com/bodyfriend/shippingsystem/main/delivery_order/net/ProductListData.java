package com.bodyfriend.shippingsystem.main.delivery_order.net;

import java.util.List;

/**
 * Created by Taeseok on 2018-02-02.
 */

public class ProductListData {
    public int resultCode;
    public String resultMsg;
    public resultData resultData;

    public class resultData {
        public int total;
        public List<ProductInfo> resultData;

        public class ProductInfo {
            public String productName;
            public String productCode;
        }
    }
}
