package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class appModifyShippingDiv extends BFEnty {

    /**
     * @param m_shippingseq
     * @param m_promise
     * @param m_promisetime
     * @param m_promisefailcd
     * @param promisefailps
     */
    public appModifyShippingDiv(String m_shippingseq, String m_promise, String m_promisetime, String m_promisefailcd, String promisefailps) {
        setUrl("mobile/api/appModifyShippingDiv.json");
        setParam("m_shippingseq", m_shippingseq, "m_promise", m_promise, "m_promisetime", m_promisetime, "m_promisefailcd", m_promisefailcd, "m_promisefailps", promisefailps);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }

    // - 완료등록(appModifyShippingDiv.json) 파라미터 추가
    // m_completenm : 설치여부명
    // m_productname : 제품명
    // m_productcode : 제품코드
    // m_custname : 고객명
}
