package com.bodyfriend.shippingsystem.main.shipping_list.net;

import android.os.Parcel;
import android.os.Parcelable;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * 배송 차량번호 리스트 및 조회 API
 */
public class appCarList extends BFEnty {

    public appCarList(String car_number) {
        setUrl("mobile/api/appCarList.json");
        setParam("car_number", car_number);
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;

        public static class resultData implements Parcelable {
            public String CAR_NUMBER;
            public String CAR_GROUP;
            public String CAR_SEQ;
            public String ADMIN_INFO;

            protected resultData(Parcel in) {
                CAR_NUMBER = in.readString();
                CAR_GROUP = in.readString();
                CAR_SEQ = in.readString();
                ADMIN_INFO = in.readString();
            }

            public final static Creator<resultData> CREATOR = new Creator<resultData>() {
                @Override
                public resultData createFromParcel(Parcel in) {
                    return new resultData(in);
                }

                @Override
                public resultData[] newArray(int size) {
                    return new resultData[size];
                }
            };

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(CAR_NUMBER);
                dest.writeString(CAR_GROUP);
                dest.writeString(CAR_SEQ);
                dest.writeString(ADMIN_INFO);
            }

            @Override
            public String toString() {
                return "ResultData{" +
                        "CAR_NUMBER='" + CAR_NUMBER + '\'' +
                        ", CAR_GROUP='" + CAR_GROUP + '\'' +
                        ", CAR_SEQ='" + CAR_SEQ + '\'' +
                        ", ADMIN_INFO='" + ADMIN_INFO + '\'' +
                        '}';
            }
        }
    }

}
