package com.bodyfriend.shippingsystem.main.product.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/*
 * 제품 리스트
 * @author 이주영
 *
 */
public class appProductList extends BFEnty {

	/**
	 * @param producttype
	 *            제품타입
	 * @param productname
	 *            제품명
	 */
	public appProductList(String producttype, String productname) {
		setUrl("mobile/api/appProductList.json");
		setParam("producttype", producttype, "productname", productname);
	}

	public Data data;

	public static class Data {
		public resultData resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public String resultMsg;
			public int resultCode;
			public int listCount;
			public List<list> list;

			public class list {
				public String PRODUCT_IMAGE_S; // 이미지주소
				public String PRODUCT_TYPE; // 제품 타입
				public String PRODUCT_NAME; // 제품명 
//				public String PRODUCT_CAUTION; // 설치주의사항
				public String PRODUCT_SPEC; // 제품스펙
				public String PRODUCT_CODE; // 제품 코드 
				public String PRODUCT_DETAIL; // 제품설명
				public String imageone;
				public String imagetwo;
			}
		}
	}

	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "ResultData": {
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "listCount": 1,
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "list": [
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): {
	// 05-07 11:28:16.649: W/_NETLOG_IN(22123): "PRODUCT_IMAGE_S": "pb_l.jpg",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_TYPE": "M",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_NAME": "아이로보(그레이)",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_CAUTION": "설치주의사항",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_SPEC": "제품스펙",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_CODE": "0000000004",
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): "PRODUCT_DETAIL": "제품설명"
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): }
	// 05-07 11:16:25.194: W/_NETLOG_IN(20728): ]

}
