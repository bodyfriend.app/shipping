package com.bodyfriend.shippingsystem.main.delivery_order.net;

import java.util.ArrayList;

/**
 * Created by Taeseok on 2018-03-05.
 */

public class OutLoiList {
    public  ArrayList<list> list;

    public  class list {
        public String deliveryManId;
        public String deliveryManNm;
        public String loiKnd;
        public String loiNo;
        public String procStatCd;
        public String regDt;
        public String vhclNo;
        public String whsCd;
        public String modelTotal;
        public String procStatNm;


        public ArrayList<models> models;

        public  class models {
            public String qty;
            public String itemExpln;
            public String gdsStatNm;
        }
    }
}
