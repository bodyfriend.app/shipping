package com.bodyfriend.shippingsystem.main.shipping_list.net;

import android.text.TextUtils;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class updateDueDate extends BFEnty {
    public updateDueDate(String m_shippingseq, String m_duedate) {
//        if (!TextUtils.isEmpty(m_duedate)) {
            setUrl("mobile/api/appModifyDueDate.json");
//        } else {
//            setUrl("mobile/api/appModifyShippingDiv.json");
//        }
        setParam("m_shippingseq", m_shippingseq, // "G201504-0029",
                "m_duedate", m_duedate // "G201504-0029",
        );
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }

}
