package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * 공지리스트
 */
public class NoticeLog extends BFEnty {

    public NoticeLog(int notiSeq) {
        setUrl("mobile/api/appNoticeLog.json");
        setParam("sess_group_cd", "SP");
        setParam("notice_seq", notiSeq);
    }

    public appNoticeList.Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;

    }
}
