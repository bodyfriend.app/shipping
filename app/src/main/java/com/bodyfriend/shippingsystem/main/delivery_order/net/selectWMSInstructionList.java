package com.bodyfriend.shippingsystem.main.delivery_order.net;


import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.base.NetConst;

import java.util.List;

/**
 * Created by 이주영 on 2016-12-08.
 */

public class selectWMSInstructionList extends BFEnty {

    public selectWMSInstructionList(String date) {
        setUrl("mobile/api/selectWMSInstructionList.json");
        setParam(
                "date", date
                , "s_producttype", NetConst.s_producttype
                , "s_oneself", "Y"
        );
    }

    public Data data;

    public static class Data {
        public resultData resultData;

        public int resultCode;
        public String resultMsg;

        public static class resultData {
            public int listCount;
            public List<list> list;

            public static class list {
                public String ITEM_DATE; // 서버에서 내려주는건 아니고 뷰 홀더에서 사용하려고 만들었다.
                public String PRODUCT_NAME;
                public String FREEGIFT_ONE;
                public String PROMISE_FAIL_CD;
                public String PURCHASING_OFFICE;
                public String INS_COMPLETE;
                public String DELIVERYMAN;
                public String DELIVERYMAN1;
                public String MUST_FLAG;
                public String QTY;
                public String WHSEQ;
                public String HPHONE_NO;
                public String BACK_SN;
                public String PROMISE;
                public String AREA;
                public String PROMISE_TIME;
                public String PRODUCTCODE;
                public String PROGRESS_NO;
                public String DUE_DATE;
                public String TEL_NO;
                public String SHIPPING_SEQ;
                public String DIV_DATE;
                public String SHIPPING_TYPE;
                public String SERVICE_MONTH;
                public String INSADDR;
                public String EXPIRE_FLAG;
                public String CUST_NAME;
                public String CARNO;
                public String PROMISE_FAIL_PS;
                public String SHIPPING_PS;
                public String FREEGIFTCODE_ONE;
                public String FREEGIFTCODE_TWO;
                public String INSTRUCTION_KEY_VALUE;
                public String INSTRUCTION_KEY; // 출고지시서 생성 키

                public String WHSEQ_NAME;
                public String FREEGIFT_NAME;
                public String L_FREEGIFT;
                public String IS_RESERVE;

                public String PRODUCT_TYPE; // 제품타입 L일경우 L_FREEGIFT가 null이면 알럿을띄운다.

                @Override
                public String toString() {
                    return "list{" +
                            "ITEM_DATE='" + ITEM_DATE + '\'' +
                            ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                            ", FREEGIFT_ONE='" + FREEGIFT_ONE + '\'' +
                            ", PROMISE_FAIL_CD='" + PROMISE_FAIL_CD + '\'' +
                            ", PURCHASING_OFFICE='" + PURCHASING_OFFICE + '\'' +
                            ", INS_COMPLETE='" + INS_COMPLETE + '\'' +
                            ", DELIVERYMAN='" + DELIVERYMAN + '\'' +
                            ", DELIVERYMAN1='" + DELIVERYMAN1 + '\'' +
                            ", MUST_FLAG='" + MUST_FLAG + '\'' +
                            ", QTY='" + QTY + '\'' +
                            ", WHSEQ='" + WHSEQ + '\'' +
                            ", HPHONE_NO='" + HPHONE_NO + '\'' +
                            ", BACK_SN='" + BACK_SN + '\'' +
                            ", PROMISE='" + PROMISE + '\'' +
                            ", AREA='" + AREA + '\'' +
                            ", PROMISE_TIME='" + PROMISE_TIME + '\'' +
                            ", PRODUCTCODE='" + PRODUCTCODE + '\'' +
                            ", PROGRESS_NO='" + PROGRESS_NO + '\'' +
                            ", DUE_DATE='" + DUE_DATE + '\'' +
                            ", TEL_NO='" + TEL_NO + '\'' +
                            ", SHIPPING_SEQ='" + SHIPPING_SEQ + '\'' +
                            ", DIV_DATE='" + DIV_DATE + '\'' +
                            ", SHIPPING_TYPE='" + SHIPPING_TYPE + '\'' +
                            ", SERVICE_MONTH='" + SERVICE_MONTH + '\'' +
                            ", INSADDR='" + INSADDR + '\'' +
                            ", EXPIRE_FLAG='" + EXPIRE_FLAG + '\'' +
                            ", CUST_NAME='" + CUST_NAME + '\'' +
                            ", CARNO='" + CARNO + '\'' +
                            ", PROMISE_FAIL_PS='" + PROMISE_FAIL_PS + '\'' +
                            ", SHIPPING_PS='" + SHIPPING_PS + '\'' +
                            ", FREEGIFTCODE_ONE='" + FREEGIFTCODE_ONE + '\'' +
                            ", FREEGIFTCODE_TWO='" + FREEGIFTCODE_TWO + '\'' +
                            ", INSTRUCTION_KEY_VALUE='" + INSTRUCTION_KEY_VALUE + '\'' +
                            ", INSTRUCTION_KEY='" + INSTRUCTION_KEY + '\'' +
                            ", WHSEQ_NAME='" + WHSEQ_NAME + '\'' +
                            ", FREEGIFT_NAME='" + FREEGIFT_NAME + '\'' +
                            ", L_FREEGIFT='" + L_FREEGIFT + '\'' +
                            ", PRODUCT_TYPE='" + PRODUCT_TYPE + '\'' +
                            '}';
                }
            }
        }
    }

//            "PRODUCT_NAME": "아이로보(아이보리)",
//            "FREEGIFT_ONE": "",
//            "PROMISE_FAIL_CD": "",
//            "PURCHASING_OFFICE": "",
//            "INS_COMPLETE": "",
//            "DELIVERYMAN": "이원주",
//            "QTY": "1",
//            "MUST_FLAG": "N",
//            "WHSEQ": "",
//            "HPHONE_NO": "0477778888",
//            "PROMISE": "Y",
//            "BACK_SN": "",
//            "PROMISE_TIME": "1414",
//            "AREA": "제주",
//            "PROGRESS_NO": "1",
//            "PRODUCTCODE": "0000000245",
//            "DUE_DATE": "2016-12-10",
//            "TEL_NO": "0477778888",
//            "SHIPPING_SEQ": "20161209M00014",
//            "SERVICE_MONTH": "  ",
//            "SHIPPING_TYPE": "배송",
//            "DIV_DATE": "2016-12-09",
//            "EXPIRE_FLAG": "BLACK",
//            "INSADDR": "제주시 제주구 제주동 400-400 400층",
//            "CUST_NAME": "곽정철",
//            "CARNO": "0",
//            "PROMISE_FAIL_PS": "",
//            "SHIPPING_PS": ""
}
